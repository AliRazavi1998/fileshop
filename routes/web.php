<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'Admin'],function(){
    Route::get('/','HomeController@index')->name('admin.home');
    Route::get('/users','UserController@index')->name('admin.user.all');
    Route::post('/user/store','UserController@store')->name('admin.user.store');
    Route::get('/user/create','UserController@create')->name('admin.user.create');
    Route::get('/user/delete/{id}','UserController@delete')->name('admin.user.delete');
    Route::get('/user/edit/{id}','UserController@edit')->name('admin.user.edit');
    Route::post('/user/edit/{id}','UserController@update')->name('admin.user.update');
});

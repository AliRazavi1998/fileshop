<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|min:3|unique:users,name',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6|max:25',
        ];
    }

    public function messages()
    {
        return[
            'username.required'=>'فیلد نام اجباری می باشد',
            'username.min'=>'    حداقل نام کاربری باید بیشتراز 3 کارکتر باشد',
            'username.unique'=>'   نام کاربری در سیستم موجود می باشد',
            'email.required'=>'فیلد ایمیل اجباری می باشد',
            'email.email'=>'   فرمت ایمیل اجباری می باشد ',
            'email.unique'=>'    ایمیل در سیستم موجود می باشد ',
            'password.required'=>'فیلد پسورد اجباری می باشد',
            'password.min'=>'حداقل پسورد  باید بیشتراز 6 کارکتر باشد',
            'password.max'=>'حداکثر  پسورد باید بیشتراز 25 کارکتر باشد',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|min:3',
            'email'=>'required|email',
        ];
    }

    public function messages()
    {
        return[
            'username.required'=>'فیلد نام اجباری می باشد',
            'username.min'=>'    حداقل نام کاربری باید بیشتراز 3 کارکتر باشد',
            'email.required'=>'فیلد ایمیل اجباری می باشد',
            'email.email'=>'   فرمت ایمیل اجباری می باشد ',
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAdminRequest;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(UserAdminRequest $request)
    {


        $user = new User();

        $user->name = $request->input('username');
        $user->email = $request->input('email');
        $user->password = md5($request->input('password'));
        $user->role = $request->input('role');
        $user->wallet = $request->input('wallet');


        $user->save();


        return redirect()->back()->with('success', 'کاربر مدنظر با موفقیت اضافه گردید');
    }

    public function delete($user_id)
    {
        if ($user_id && ctype_digit($user_id)) {
            User::where('id', $user_id)->delete();


            return redirect(route('admin.user.all'))->with('success', 'کاربر مدنظر با موفقیت حذف گردید');
        }

        return redirect(route('admin.user.all'))->with('error', 'مشکل در عملیات اجرایی وب سایت رخ داده است لطفا مجددا تلاش کنید');
    }

    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.user.edit', compact('user'));
    }

    public function update(UpdateRequest $request, $user_id)
    {
        $input = [
            'name' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'role' => $request->input('role'),
            'wallet' => $request->input('wallet'),
        ];

        if (empty(\request()->input('password'))) {
            unset($input['password']);
        }



        $user = User::find($user_id);


        $user->update($input);

        return redirect()->back()->with('success', 'کاربر شما با موفقیت آبدیت شد');
    }
}

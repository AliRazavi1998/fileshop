<div class="row">

    <div class="col-xl-12 col-md-6">
        @include('partials.error')
        @include('partials.noti')
        <form action="{{route('admin.user.store')}}" method="POST">
            @csrf
            <div class="form-group">

                <label for="username">نام کاربری : </label>

                <input class="form-control @error('username') is-invalid @enderror" type="text" name="username"
                       placeholder="نام کاربری " value="{{old('username',isset($user) ? $user->name : '')}}"/>
            </div>

            <div class="form-group">

                <label for="email">ایمیل : </label>

                <input class="form-control @error('email') is-invalid @enderror" type="email" name="email"
                       placeholder="ایمیل" value="{{old('email',isset($user) ? $user->email : '')}}"/>
            </div>

            <div class="form-group">

                <label for="password"> پسورد : </label>

                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password"
                       placeholder="پسورد " />
            </div>

            <div class="form-group">

                <label for="role"> نقش کاربری : </label>

                <select name="role" id="role" class="form-control">
                    <option value="1" {{isset($user) && $user->role==1 ? 'selected' : ''}}>کاربر عادی</option>
                    <option value="2" {{isset($user) && $user->role==2 ? 'selected' : ''}}> پشتیبان</option>
                    <option value="3" {{isset($user) && $user->role==3 ? 'selected' : ''}}> مدیریت</option>
                </select>

            </div>
            <div class="form-group">

                <label for="wallet">مقدار موجودی : </label>

                <input class="form-control" type="number" name="wallet" placeholder="مقدار موجودی   "
                       value="{{old('wallet',isset($user) ? $user->wallet : 0)}}"/>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="ذخیره اطلاعات" name="submit_save_information">
            </div>
        </form>
    </div>
</div>

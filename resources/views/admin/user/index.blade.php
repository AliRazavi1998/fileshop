@extends('layouts.master')


@section('content')
    <div class="col-sm-12 col-md-12 mt-5">
        @include('partials.noti')
        <div class="card">
            <div class="card-header">لیست کاربران</div>
            <div class="card-body">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کاربری</th>
                        <th>ایمیل</th>
                        <th>مقدار موجودی</th>
                        <th>نقش کاربری</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{++$loop->index }}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->wallet}}</td>
                            <td>
                                @if($user->role==1)

                                    <span class="badge badge-info">کاربر عادی</span>

                                @elseif($user->role==2)

                                    <span class="badge badge-primary">کاربر پشتیبان</span>

                                @elseif($user->role==3)
                                    <span class="badge badge-primary">کاربر مدیر </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.user.delete',$user->id)}}" class="btn btn-sm btn-danger">حذف</a>
                                <a href="{{route('admin.user.edit',$user->id)}}"
                                   class="btn btn-sm btn-warning">ویرایش</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>


    </div>
@endsection

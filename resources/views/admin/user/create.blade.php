@extends('layouts.master')


@section('content')
    <div class="col-sm-12 col-md-12 mt-5">
        <div class="card">
            <div class="card-header">ساخت  کاربر جدید</div>
            <div class="card-body">
                 @include('admin.user.form')
            </div>
       
        </div>
    </div>
@endsection
